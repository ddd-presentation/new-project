<?php

namespace Domain\File;

class FilePersistProcessor
{
    private FilePersistGuard $fileGuard;
    private FileRepository $fileRepository;
    private FileReader $fileProvider;

    public function __construct(
        FilePersistGuard         $fileGuard,
        FileRepository           $fileRepository,
        FileReader               $fileProvider
    ) {
        $this->fileGuard = $fileGuard;
        $this->fileProvider = $fileProvider;
        $this->fileRepository = $fileRepository;
    }

    public function persist(File $file): void
    {
        $this->fileGuard->guard($file);
        $this->fileProvider->write($file);
        $this->fileRepository->save($file);
    }
}