<?php

namespace Domain\File;

use MongoDB\BSON\ObjectId;

class File
{
    private ObjectId $id;
    private string $path;
    private string $ext;
    private int $size;
    private ?string $context;

    public function __construct(
        ObjectId $id,
        string $path,
        string $ext,
        string $size,
        ?string $context = null
    ) {
        $this->id = $id;
        $this->path = $path;
        $this->ext = $ext;
        $this->size = $size;
        $this->context = $context;
    }

    public function getId(): ObjectId
    {
        return $this->id;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    public function getExt(): string
    {
        return $this->ext;
    }

    public function setExt(string $ext): void
    {
        $this->ext = $ext;
    }

    public function getContext(): ?string
    {
        return $this->context;
    }

    public function setContext(?string $context): void
    {
        $this->context = $context;
    }
}