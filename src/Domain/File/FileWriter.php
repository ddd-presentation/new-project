<?php

namespace Domain\File;

interface FileWriter
{
    public function write(File $file): void;
}