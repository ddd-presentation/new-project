<?php

namespace Domain\File;

use Domain\File\FileSpecification\FileSizeMoreThenValueSpecification;
use Domain\File\FileSpecification\FileSpecCollection;

class FileRetriever
{
    private const FILE_SELECTION_SIZE = 2;

    private FileRepository $fileRepository;

    public function __construct(
        FileRepository $fileRepository
    ) {
        $this->fileRepository = $fileRepository;
    }

    public function getFilesLargerThen2MB(): array
    {
        return $this->fileRepository->get(
            new FileSpecCollection([
                new FileSizeMoreThenValueSpecification(self::FILE_SELECTION_SIZE)
            ])
        );
    }
}