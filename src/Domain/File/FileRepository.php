<?php

namespace Domain\File;

use Domain\File\FileSpecification\FileSpecCollection;

interface FileRepository
{
    /** @return File[] */
    public function get(FileSpecCollection $collection): array;

    public function save(File $file): void;
}