<?php

namespace Domain\File;

interface FileReader
{
    public function read(File $file): ?string;
}