<?php

namespace Domain\File;

use Domain\File\Exception\FileExtException;
use Domain\File\Exception\FileSizeException;

class FilePersistGuard
{
    public function __construct() {}

    public function guard(File $file): void
    {
        $this->guardFileSize($file);
        $this->guardFileExt($file);
    }

    public function guardFileSize(File $file): void
    {
        /**
         * ...
         **/

        throw new FileSizeException();
    }

    public function guardFileExt(File $file): void
    {
        /**
         * ...
         **/

        throw new FileExtException();
    }
}