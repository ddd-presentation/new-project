<?php

namespace Domain\File\FileSpecification;

class FileSizeMoreThenValueSpecification implements FileSpecification
{
    private int $size;

    public function __construct(int $size) {
        $this->size = $size;
    }

    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    public function getSize(): int
    {
        return $this->size;
    }
}