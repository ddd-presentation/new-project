<?php

namespace Domain\File\FileSpecification;

class FileSpecCollection
{
    /** @var FileSpecification[]  */
    private array $specs;

    public function __construct(
        array $specs
    ) {
        $this->specs = $specs;
    }

    /**
     * @return FileSpecification[]
     */
    public function getSpecs(): array
    {
        return $this->specs;
    }
}