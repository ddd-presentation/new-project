<?php

namespace Infrastructure\LocalStorage\File;

use Domain\File\File;
use Domain\File\FileReader;
use Domain\File\FileWriter;
use Infrastructure\Exception\InfrastructureException;

class LocalStorageFileProvider implements FileReader, FileWriter
{
    public function read(File $file): ?string
    {
        // TODO: Implement read() method.
        throw new InfrastructureException('Method not supported');
    }

    public function write(File $file): void
    {
        // TODO: Implement write() method.
        throw new InfrastructureException('Method not supported');
    }
}