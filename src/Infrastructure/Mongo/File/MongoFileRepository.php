<?php

namespace Infrastructure\Mongo\File;

use Domain\File\File;
use Domain\File\FileRepository;
use Domain\File\FileSpecification\FileSpecCollection;
use Infrastructure\Exception\InfrastructureException;

class MongoFileRepository implements FileRepository
{
    public function get(FileSpecCollection $collection): array
    {
        throw new InfrastructureException('Method not supported');
    }

    public function save(File $file): void
    {
        throw new InfrastructureException('Method not supported');
    }
}