<?php

namespace Application\File;

use Application\Request;
use Application\Response;
use Domain\File\Exception\FileDomainException;
use Domain\File\File;
use Domain\File\FilePersistProcessor;
use Domain\File\FileReader;
use Exception\ApplicationException;
use MongoDB\BSON\ObjectId;

class FilePutController
{
    private FileReader $fileReader;
    private FilePersistProcessor $persistProcessor;

    public function __construct(
        FileReader $fileProvider,
        FilePersistProcessor $persistProcessor
    ) {
        $this->fileReader = $fileProvider;
        $this->persistProcessor = $persistProcessor;
    }

    public function __invoke(Request $request): Response
    {
        $fileArray = $request->getFiles();
        $file = new File(
            new ObjectId(),
            $fileArray['path'],
            $fileArray['ext'],
            $fileArray['size']
        );
        $file->setContext($this->fileReader->read($file));

        try {
            $this->persistProcessor->persist($file);
        } catch (FileDomainException $exception) {
            throw new ApplicationException();
        }

        return new Response();
    }
}