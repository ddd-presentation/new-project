<?php

namespace Application\File;

use Application\Request;
use Application\Response;
use Domain\File\FileRetriever;

class FileGetController
{
    private FileRetriever $fileRetriever;

    public function __construct(
        FileRetriever $fileRetriever
    ) {
        $this->fileRetriever = $fileRetriever;
    }

    public function __invoke(Request $request): Response
    {
        return new Response(
            $this->fileRetriever->getFilesLargerThen2MB()
        );
    }
}